<?php

namespace Abc\Bundle\LogBundle\Controller;

use Abc\Bundle\LogBundle\Model\EventLogList;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\FOSRestController;

use Abc\Bundle\LogBundle\Model\EventLogManagerInterface;

use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class LogController
 *
 * @author Wojciech Ciolko <w.ciolko@gmail.com>
 * @RouteResource("log")
 */
class LogController extends FOSRestController
{

    /**
     * @param ParamFetcherInterface $paramFetcher
     * @QueryParam(name="offset", requirements="\d+", default="0", description="Offset")
     * @QueryParam(name="limit", requirements="\d+", default="100", description="Page size")
     * @QueryParam(name="sortCol", default="id", description="Sort column")
     * @QueryParam(name="sortDir", default="ASC", description="Sort direction")
     * @QueryParam(name="filters", description="Searching criteria")
     * @return array data
     *
     * @Operation(
     *     tags={"Log"},
     *     summary="Returns a collection of logs",
     *     @SWG\Parameter(
     *         name="sortDir",
     *         in="query",
     *         description="Sort direction",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="offset",
     *         in="query",
     *         description="Offset",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Page size",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sortCol",
     *         in="query",
     *         description="Sort column",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="filters",
     *         in="query",
     *         description="Searching criteria",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @Model(type="Abc\Bundle\LogBundle\Model\EventLogList")
     *     )
     * )
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        $offset     = $paramFetcher->get('offset');
        $sortColumn = $paramFetcher->get('sortCol');
        $sortDir    = $paramFetcher->get('sortDir');
        $limit      = $paramFetcher->get('limit');
        $criteria   = $paramFetcher->get('filters');

        if (!$criteria) {
            $criteria = [];
        }

        $criteria = $this->filterCriteria($criteria);
        $manager  = $this->getEventLogManager();
        $entities = $manager->findBy($criteria, [$sortColumn => $sortDir], $limit, $offset);

        $list = new EventLogList();
        $list->setItems($entities);

        return $list;
    }

    /**
     * @param $criteria
     * @return array
     */
    protected function filterCriteria($criteria)
    {
        foreach ($criteria as $key => $value) {
            if ($criteria[$key] == '') {
                unset ($criteria[$key]);
            }
        }
        return $criteria;
    }

    /**
     * @return EventLogManagerInterface
     */
    private function getEventLogManager()
    {
        return $this->get('abc_log.event_log_manager');
    }
}