<?php

namespace Abc\Bundle\LogBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;
/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AbcLogExtension extends Extension
{
    const NAMESPACE_PREFIX = 'abc_log.';

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $fileLocator = new FileLocator(__DIR__ . '/../Resources/config/services');

        $container->setAlias('abc_log.event_log_manager', $config['service']['event_log_manager']);

        if ('custom' !== $config['db_driver']) {
            if('odm' === $config['db_driver']) {
                $loader = new Loader\YamlFileLoader($container, $fileLocator);
                $loader->load('services.yml');
                $loader->load(sprintf('%s.yml', $config['db_driver']));
            } elseif ('elastic' === $config['db_driver']) {
                $loader = new Loader\YamlFileLoader($container, $fileLocator);
                $loader->load('services.yml');
                $loader->load(sprintf('%s.yml', $config['db_driver']));
            } elseif ('elastic_v2' === $config['db_driver']) {
                $loader = new Loader\YamlFileLoader($container, $fileLocator);
                $loader->load('services.yml');
                $loader->load(sprintf('%s.yml', $config['db_driver']));
            } else {
                $loader = new Loader\XmlFileLoader($container, $fileLocator);
                $loader->load(sprintf('%s.xml', $config['db_driver']));
            }
        }

        $loader = new Loader\XmlFileLoader($container, $fileLocator);
        $loader->load('services.xml');

        $this->remapParametersNamespaces(
            $config,
            $container,
            array(
                '' => array(
                    'model_manager_name' => self::NAMESPACE_PREFIX . 'model_manager_name',
                )
            )
        );
    }

    protected function remapParameters(array $config, ContainerBuilder $container, array $map)
    {
        foreach ($map as $name => $paramName) {
            if (array_key_exists($name, $config)) {
                $container->setParameter($paramName, $config[$name]);
            }
        }
    }

    protected function remapParametersNamespaces(array $config, ContainerBuilder $container, array $namespaces)
    {
        foreach ($namespaces as $ns => $map) {
            if ($ns) {
                if (!array_key_exists($ns, $config)) {
                    continue;
                }
                $namespaceConfig = $config[$ns];
            } else {
                $namespaceConfig = $config;
            }
            if (is_array($map)) {
                $this->remapParameters($namespaceConfig, $container, $map);
            } else {
                foreach ($namespaceConfig as $name => $value) {
                    $container->setParameter(sprintf($map, $name), $value);
                }
            }
        }
    }
}
