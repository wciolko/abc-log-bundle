<?php

namespace Abc\Bundle\LogBundle\Service;

use Abc\Bundle\LogBundle\Entity\EventLog;
use Abc\Bundle\LogBundle\Model\AuthManagerInterface;
use Abc\Bundle\LogBundle\Model\EventLogInterface;
use Abc\Bundle\LogBundle\Model\EventLogManagerInterface;
use DateTime;

/**
 * Class GeneralLogService
 *
 */
class GeneralLogService
{
    /** @var EventLogManagerInterface */
    protected $eventLogManager;

    /** @var AuthManagerInterface */
    protected $authManager;

    /**
     * @param EventLogManagerInterface $eventLogManager
     * @param AuthManagerInterface     $authManager
     */
    public function __construct(EventLogManagerInterface $eventLogManager, AuthManagerInterface $authManager = null)
    {
        $this->eventLogManager = $eventLogManager;
        $this->authManager     = $authManager;
    }

    /**
     * @param string $referenceId
     * @param string $type
     * @param string $description
     * @param array  $parameters
     */
    public function logEvent($referenceId, $type, $description, array $parameters = [])
    {
        $createdBy = 'system';
        if ($this->authManager) {
            $user = $this->authManager->getUser();
            if ($user) {
                $createdBy = $user->getUsername();
            }
        }

        $eventLog = new EventLog();
        $eventLog->setReferenceId($referenceId);
        $eventLog->setType($type);
        $eventLog->setDescription($description);
        $eventLog->setParameters($parameters);
        $eventLog->setCreatedBy($createdBy);
        $eventLog->setCreatedAt(new DateTime());
        $eventLog->setUpdatedAt(new DateTime());


        $this->eventLogManager->update($eventLog);
    }

    /**
     * @param EventLogInterface $logEntry
     */
    public function logEventObject(EventLogInterface $logEntry)
    {
        $createdBy = $logEntry->getCreatedBy();
        $createdAt = $logEntry->getCreatedAt() ? : new DateTime();
        
        if (!$logEntry) {
            $createdBy = 'system';
        }
        
        if ($this->authManager) {
            $user = $this->authManager->getUser();
            if ($user) {
                $createdBy = $user->getUsername();
            }
        }

        $logEntry->setCreatedBy($createdBy);
        $logEntry->setCreatedAt($createdAt);
        $logEntry->setUpdatedAt(new DateTime());


        $this->eventLogManager->update($logEntry);
    }
}