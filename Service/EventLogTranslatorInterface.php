<?php

namespace Abc\Bundle\LogBundle\Service;

use Abc\Bundle\LogBundle\Model\EventLogInterface;

interface EventLogTranslatorInterface
{
    /**
     * Translates the event log and assign the respective translatedDescription property.
     * Does not persist the entity, however.
     *
     * @param EventLogInterface $eventLog
     */
    public function translate(EventLogInterface $eventLog);
}