<?php

namespace Abc\Bundle\LogBundle\Service;

use Abc\Bundle\LogBundle\Model\EventLogInterface;
use Symfony\Component\Translation\TranslatorInterface;

class EventLogTranslator implements EventLogTranslatorInterface
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function translate(EventLogInterface $eventLog)
    {
        if (empty($eventLog->getDescription())) {
            $eventLog->setTranslatedDescription([]);
            return $eventLog;
        }

        $originalLocale = $this->translator->getLocale();
        $translations   = [];

        foreach ($this->getLocales() as $locale) {
            $this->translator->setLocale($locale);

            $params = [];
            foreach ($eventLog->getParameters() as $key => $value) {
                $params['%' . $key . '%'] = $value;
            }

            $translations[$locale] = $this->translator->trans($eventLog->getDescription(), $params);
        }

        $eventLog->setTranslatedDescription($translations);
        $this->translator->setLocale($originalLocale);
    }

    /**
     * @return array
     */
    private function getLocales()
    {
        // warning: the unit test currently expects a single locale, adapt it if you add a second one!
        return ['pl'];
    }

}