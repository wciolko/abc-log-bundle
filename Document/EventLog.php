<?php

namespace Abc\Bundle\LogBundle\Document;

use Abc\Bundle\LogBundle\Model\EventLog as BaseEventLog;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class EventLog
 *
 * @author  wojciechciolko <>
 * @package Abc\Bundle\LogBundle\Document
 * @MongoDB\Document()
 */
class EventLog extends BaseEventLog
{
    /**
     * @var string $id
     * @MongoDB\Id
     * @Serializer\Type("string")
     */
    protected $id;

    /**
     * @var string $type
     * @MongoDB\Field(type="string")
     * @Serializer\Type("string")
     */
    protected $type;

    /**
     * @var string $description
     * @MongoDB\Field(type="string")
     * @Serializer\Type("string")
     *
     */
    protected $description;

    /**
     * @Serializer\Type("array")
     * @Serializer\SerializedName("translatedDescription")
     * @MongoDB\Field(type="hash")
     *
     * @var array
     */
    protected $translatedDescription = [];

    /**
     * @var \DateTime $createdAt
     * @MongoDB\Field(type="date")
     * @Serializer\Type("DateTime")
     * @Serializer\SerializedName("createdAt")
     */
    protected $createdAt;

    /**
     * @var \DateTime $updatedAt
     * @MongoDB\Field(type="date")
     * @Serializer\Type("DateTime")
     * @Serializer\SerializedName("updatedAt")
     */
    protected $updatedAt;

    /**
     * @var string $referenceId
     * @MongoDB\Field(type="string")
     * @Serializer\Type("string")
     * @Serializer\SerializedName("referenceId")
     */
    protected $referenceId;

    /**
     * @var string $createdBy
     * @MongoDB\Field(type="string")
     * @Serializer\Type("string")
     * @Serializer\SerializedName("createdBy")
     */
    protected $createdBy;

    /**
     * @var array $parameters
     * @MongoDB\Field(type="hash")
     * @Serializer\Type("array")
     */
    protected $parameters = [];
}