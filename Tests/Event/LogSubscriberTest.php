<?php


namespace Abc\Bundle\LogBundle\Tests\Event;


use Abc\Bundle\LogBundle\Event\LogEvent;
use Abc\Bundle\LogBundle\Event\LogEvents;
use Abc\Bundle\LogBundle\Event\LogSubscriber;
use Abc\Bundle\LogBundle\Service\GeneralLogService;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class LogSubscriberTest extends \PHPUnit_Framework_TestCase
{
    /** @var GeneralLogService|\PHPUnit_Framework_MockObject_MockObject */
    protected $logService;
    /** @var LogSubscriber */
    protected $subject;
    /** @var LoggerInterface|\PHPUnit_Framework_MockObject_MockObject */
    private $logger;

    public function setUp()
    {
        $this->logService = $this->getMockBuilder('Abc\Bundle\LogBundle\Service\GeneralLogService')->disableOriginalConstructor()->getMock();
        $this->logger     = $this->getMock('Symfony\Component\HttpKernel\Log\LoggerInterface');
        $this->subject    = new LogSubscriber($this->logService, $this->logger);
    }

    public function testGetSubscribedEvents()
    {
        $result = LogSubscriber::getSubscribedEvents();
        $this->assertEquals(
            [
                LogEvents::LOG_CREATE => ['logCreate', 0],
            ],
            $result
        );
    }

    public function testConstructorWithEmptyLogger()
    {
        $subject = new LogSubscriber($this->logService);
        $this->assertInstanceOf('Abc\Bundle\LogBundle\Event\LogSubscriber', $subject);
    }

    public function testLogCreateWithValidDataWillCreateLog()
    {
        list($eventLog, $logEvent) = $this->getEventExpectations();
        $this->logService->expects($this->once())
            ->method('logEventObject')
            ->with($eventLog);

        $this->subject->logCreate($logEvent);
    }

    public function testLogCreateThrowsException()
    {
        list($eventLog, $logEvent) = $this->getEventExpectations();
        $e = new \Exception('Test message');
        $this->logger->expects($this->once())
            ->method('debug');
        $this->logger->expects($this->once())
            ->method('info');
        $this->logger->expects($this->once())
            ->method('error');
        $this->logService->expects($this->once())
            ->method('logEventObject')
            ->with($eventLog)
            ->willThrowException($e);

        $this->subject->logCreate($logEvent);
    }

    public function testLogCreateWithInvalidDataWillNotCreateLog()
    {
        /** @var LogEvent|\PHPUnit_Framework_MockObject_MockObject $logEvent */
        $logEvent = $this->getMockBuilder('Abc\Bundle\LogBundle\Event\LogEvent')->disableOriginalConstructor()->getMock();
        $logEvent->expects($this->once())
            ->method('getEventLog')
            ->willReturn($this->logger);

        $this->logService->expects($this->never())
            ->method('logEventObject');

        $this->subject->logCreate($logEvent);
    }

    /**
     * @return array
     */
    protected function getEventExpectations()
    {
        $eventLog = $this->getMock('Abc\Bundle\LogBundle\Model\EventLogInterface');
        /** @var LogEvent|\PHPUnit_Framework_MockObject_MockObject $logEvent */
        $logEvent = $this->getMockBuilder('Abc\Bundle\LogBundle\Event\LogEvent')->disableOriginalConstructor()->getMock();
        $logEvent->expects($this->once())
            ->method('getEventLog')
            ->willReturn($eventLog);
        return array($eventLog, $logEvent);
    }

}
