<?php


namespace Abc\Bundle\LogBundle\Tests\Event;


use Abc\Bundle\LogBundle\Event\LogEvent;
use Abc\Bundle\LogBundle\Model\EventLogInterface;

class LogEventTest extends \PHPUnit_Framework_TestCase
{

    /** @var EventLogInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $eventLog;
    /** @var LogEvent */
    protected $subject;

    public function setUp()
    {
        $this->eventLog = $this->getMock('Abc\Bundle\LogBundle\Model\EventLogInterface');
        $this->subject  = new LogEvent($this->eventLog);
    }

    public function testGetEventLog()
    {
        $result = $this->subject->getEventLog();
        $this->assertEquals($this->eventLog, $result);
    }

}
