<?php

namespace Abc\Bundle\LogBundle\Tests\ElasticSearch;

use Abc\Bundle\LogBundle\ElasticSearch\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testCreate()
    {
        Client::getInstance('eu-central-1', 'arn:aws:iam::139536746838:role/LinkerESlogdev-access', ['https://linkerlogs-dev.linker.shop:443'])
            ->indices()
            ->create([
                'index' => 'event_logs_xyz',
            ]);
    }
}
