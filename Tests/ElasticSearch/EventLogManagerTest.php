<?php

namespace Abc\Bundle\LogBundle\Tests\ElasticSearch;

use Abc\Bundle\LogBundle\ElasticSearch\EventLogManager;
use Abc\Bundle\LogBundle\Model\EventLog;
use Abc\Bundle\LogBundle\Service\EventLogTranslatorInterface;
use Elasticsearch\ClientBuilder;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class EventLogManager
 * @package Abc\Bundle\LogBundle\Tests\ElasticSearch
 */
class EventLogManagerTest extends TestCase
{
    /** @var EventLogManager */
    protected $subject;
    /** @var EventLogTranslatorInterface */
    protected $eventLogTranslator;
    /** @var SerializerInterface|MockObject */
    protected $serializer;

    public function setUp(): void
    {
        $client = ClientBuilder::create()->setHosts(['localhost:9200'])->build();
        $this->eventLogTranslator = $this->createMock(EventLogTranslatorInterface::class);
        $this->serializer = $this->createMock(SerializerInterface::class);

        $this->subject = new EventLogManager($client, $this->eventLogTranslator, $this->serializer);
    }

//    public function testUpdate()
//    {
//        $eventLog = (new EventLog())->fromArray(['referenceId' => uniqid()]);
//
//        $this->subject->update($eventLog);
//    }
//
//    public function testFindOneBy()
//    {
//        $result = $this->subject->findOneBy(['referenceId' => 'abc']);
//        var_dump($result);
//    }

    public function testFindAll()
    {
        $result = $this->subject->findAll();
    }

    public function testGetTotal()
    {
        $result = $this->subject->getTotal();
    }
}
