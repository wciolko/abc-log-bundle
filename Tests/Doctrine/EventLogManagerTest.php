<?php

namespace Abc\Bundle\LogBundle\Tests\Doctrine;

use Abc\Bundle\LogBundle\Doctrine\EventLogManager;
use Abc\Bundle\LogBundle\Service\EventLogTranslator;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;

class EventLogManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var string */
    private $class;
    /** @var ClassMetadata|\PHPUnit_Framework_MockObject_MockObject */
    private $classMetaData;
    /** @var ObjectManager|\PHPUnit_Framework_MockObject_MockObject */
    private $objectManager;
    /** @var ObjectRepository|\PHPUnit_Framework_MockObject_MockObject */
    private $repository;
    /** @var EventLogTranslator|\PHPUnit_Framework_MockObject_MockObject */
    protected $logTranslator;
    /** @var EventLogManager */
    private $subject;


    public function setUp()
    {
        $this->class         = 'Abc\Bundle\LogBundle\Entity\EventLog';
        $this->classMetaData = $this->getMock('Doctrine\Common\Persistence\Mapping\ClassMetadata');
        $this->objectManager = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $this->repository    = $this->getMockBuilder('Doctrine\ORM\EntityRepository')->disableOriginalConstructor()->getMock();
        $this->logTranslator = $this->getMock('Abc\Bundle\LogBundle\Service\EventLogTranslatorInterface');

        $this->objectManager->expects($this->any())
            ->method('getClassMetadata')
            ->will($this->returnValue($this->classMetaData));

        $this->classMetaData->expects($this->any())
            ->method('getName')
            ->will($this->returnValue($this->class));

        $this->objectManager->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($this->repository));

        $this->subject = new EventLogManager($this->objectManager, $this->class, $this->logTranslator);
    }


    public function testGetClass()
    {
        $this->assertEquals($this->class, $this->subject->getClass());
    }


    public function testUpdate()
    {
        $filesystem = $this->subject->create();

        $this->objectManager->expects($this->once())
            ->method('persist')
            ->with($filesystem);

        $this->objectManager->expects($this->once())
            ->method('flush');

        $this->subject->update($filesystem);
    }


    public function testUpdateWithFlush()
    {
        $filesystem = $this->subject->create();

        $this->objectManager->expects($this->once())
            ->method('persist')
            ->with($filesystem);

        $this->objectManager->expects($this->never())
            ->method('flush');

        $this->subject->update($filesystem, false);
    }


    public function testDelete()
    {
        $filesystem = $this->subject->create();

        $this->objectManager->expects($this->once())
            ->method('remove')
            ->with($filesystem);

        $this->objectManager->expects($this->once())
            ->method('flush');

        $this->subject->delete($filesystem);
    }


    public function testFindAll()
    {
        $this->repository->expects($this->once())
            ->method('findAll');

        $this->subject->findAll();
    }


    public function testFindBy()
    {
        $criteria = ['foo'];

        $this->repository->expects($this->once())
            ->method('findBy')
            ->with($criteria);

        $this->subject->findBy($criteria);
    }

    public function testFindById()
    {
        $id = 1;
        $this->repository->expects($this->once())
            ->method('find')
            ->with($id);
        $this->subject->findById($id);
    }

    public function testFindBySlug()
    {
        $slug     = 'foo';
        $criteria = ['slug' => $slug];

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with($criteria);

        $this->subject->findBySlug($slug);
    }

    public function testGetTotalByType()
    {
        $total       = 10;
        $type        = 'typeTest';
        $referenceId = 'ref';
        $qb          = $this->getTotalExpectations($total);
        $qb->expects($this->at(1))
            ->method('setParameter')
            ->with(':referenceId', $referenceId);
        $qb->expects($this->at(2))
            ->method('setParameter')
            ->with(':type', $type);
        $qb->expects($this->once())
            ->method('where')
            ->with('el.referenceId = :referenceId AND el.type = :type');
        $result = $this->subject->getTotalByType($referenceId, $type);

        $this->assertEquals($total, $result);
    }

    public function testGetTotal()
    {
        $total = 10;
        $this->getTotalExpectations($total);

        $result = $this->subject->getTotal();

        $this->assertEquals($total, $result);
    }

    /**
     * @param $total
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getTotalExpectations($total)
    {
        $qb = $this->getMockBuilder('Doctrine\ORM\QueryBuilder')->disableOriginalConstructor()->getMock();
        $q  = $this->getMock('Abc\Bundle\LogBundle\Tests\Doctrine\Query');

        $qb->expects($this->once())
            ->method('getQuery')
            ->willReturn($q);

        $qb->expects($this->once())
            ->method('select')
            ->with('count(el.id)');

        $q->expects($this->once())
            ->method('getSingleScalarResult')
            ->willReturn($total);

        $this->repository->expects($this->once())
            ->method('createQueryBuilder')
            ->with('el')
            ->willReturn($qb);
        return $qb;
    }


}

class Query
{
    public function setParameters()
    {
    }

    public function getResult()
    {
    }

    public function getSingleScalarResult()
    {
    }
}