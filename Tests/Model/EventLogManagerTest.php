<?php


namespace Abc\Bundle\LogBundle\Tests\Model;

use Abc\Bundle\LogBundle\Model\EventLogManager;

class EventLogManagerTest extends \PHPUnit_Framework_TestCase {

    /** @var EventLogManager|\PHPUnit_Framework_MockObject_MockObject */
    protected $subject;

    public function setUp()
    {
        $this->subject = $this->getMockForAbstractClass('Abc\Bundle\LogBundle\Model\EventLogManager');
    }


    public function testCreate()
    {
        $this->subject->expects($this->any())
            ->method('getClass')
            ->will($this->returnValue('Abc\Bundle\LogBundle\Entity\EventLog'));

        $entity = $this->subject->create();

        $this->assertInstanceOf('Abc\Bundle\LogBundle\Entity\EventLog', $entity);
    }
}
 