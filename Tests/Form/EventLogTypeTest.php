<?php


namespace Abc\Bundle\LogBundle\Tests\Form;


use Abc\Bundle\LogBundle\Model\EventLog;
use Abc\Bundle\LogBundle\Form\EventLogType;
use Symfony\Component\Form\Test\TypeTestCase;

class EventLogTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = array(
            'referenceId' => 'test',
            'type'        => 'test2',
            'description' => 'test description',
            'parameters'  => 'test parameters',
            'createdBy'   => 'test createdBy',
        );

        $form = $this->factory->create(EventLogType::class);

        $object = new EventLog();
        $object->setReferenceId($formData['referenceId']);
        $object->setType($formData['type']);
        $object->setDescription($formData['description']);
        $object->setParameters($formData['parameters']);
        $object->setCreatedBy($formData['createdBy']);

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($object, $form->getData());

        $view     = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}
