<?php

namespace Abc\Bundle\LogBundle\Tests\Integration;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ServiceContainerTest extends KernelTestCase
{

    /** @var ContainerInterface */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        self::bootKernel();

        $this->container = static::$kernel->getContainer();

        $application = new Application(static::$kernel);
        $application->setAutoExit(false);
        $application->setCatchExceptions(false);
    }

    /**
     * @param string $service
     * @param string $type
     * @dataProvider getServices
     */
    public function testGetFromContainer($service, $type)
    {
        $subject = $this->container->get($service);

        $this->assertInstanceOf($type, $subject);
    }

    /**
     * @return array
     */
    public function getServices()
    {
        return [
            ['abc_log.entity_manager', 'Doctrine\ORM\EntityManager'],
            ['abc_log.event_log_manager.default', 'Abc\Bundle\LogBundle\Doctrine\EventLogManager'],
            ['abc_log.global_log_service', 'Abc\Bundle\\LogBundle\Service\GeneralLogService'],
            ['abc_log.event_log_translator', 'Abc\Bundle\LogBundle\Service\EventLogTranslator'],
            ['abc_log.event_subscriber', 'Abc\Bundle\LogBundle\Event\LogSubscriber'],
        ];
    }
}

