<?php


namespace Abc\Bundle\LogBundle\Tests\Service;


use Abc\Bundle\LogBundle\Entity\EventLog;
use Abc\Bundle\LogBundle\Model\AuthManagerInterface;
use Abc\Bundle\LogBundle\Model\EventLogManagerInterface;
use Abc\Bundle\LogBundle\Service\GeneralLogService;

class GeneralLogServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var GeneralLogService */
    private $subject;

    /** @var EventLogManagerInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $eventLogManager;

    /** @var AuthManagerInterface|\PHPUnit_Framework_MockObject_MockObject */
    protected $authManager;

    public function setUp()
    {
        $this->eventLogManager = $this->getMock('Abc\Bundle\LogBundle\Model\EventLogManagerInterface');
        $this->authManager     = $this->getMock('Abc\Bundle\LogBundle\Model\AuthManagerInterface');
        $this->subject         = new GeneralLogService($this->eventLogManager, $this->authManager);
    }

    public function testLogEventWithSystemUser()
    {
        $clientId    = 1;
        $description = 'description';
        $createdBy   = 'system';
        $eventLog    = new EventLog();
        $eventLog->setReferenceId($clientId);
        $eventLog->setCreatedBy($createdBy);

        $eventLog->setDescription($description);
        $eventLog->setParameters([]);
        $eventLog->setType('client');

        $this->eventLogManager->expects($this->once())
            ->method('update');
        $this->authManager->expects($this->once())
            ->method('getUser')
            ->willReturn(null);

        $this->subject->logEvent($clientId, $description, $createdBy, []);
    }

    public function testLogEventObjectWithSystemUser()
    {
        $clientId    = 1;
        $description = 'description';
        $createdBy   = 'system';
        $eventLog    = new EventLog();
        $eventLog->setReferenceId($clientId);
        $eventLog->setCreatedBy($createdBy);

        $eventLog->setDescription($description);
        $eventLog->setParameters([]);
        $eventLog->setType('client');

        $this->authManager->expects($this->once())
            ->method('getUser')
            ->willReturn(null);

        $this->eventLogManager->expects($this->once())
            ->method('update')
            ->with($eventLog);

        $this->subject->logEventObject($eventLog);
    }

    public function testLogEventObjectWithUser()
    {
        $clientId    = 1;
        $description = 'description';
        $createdBy   = 'system';
        $eventLog    = new EventLog();
        $eventLog->setReferenceId($clientId);
        $eventLog->setCreatedBy($createdBy);

        $eventLog->setDescription($description);
        $eventLog->setParameters([]);
        $eventLog->setType('client');

        $user = $this->getMock('Symfony\Component\Security\Core\User\UserInterface');
        $user->expects($this->once())
            ->method('getUsername')
            ->willReturn('username');

        $this->authManager->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $this->eventLogManager->expects($this->once())
            ->method('update')
            ->with($eventLog);

        $this->subject->logEventObject($eventLog);
    }
}
