<?php

namespace Abc\Bundle\LogBundle\Test\Service;

use Abc\Bundle\LogBundle\Service\EventLogTranslator;
use Abc\Bundle\LogBundle\Model\EventLog;
use Symfony\Component\Translation\TranslatorInterface;

class EventLogTranslatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var EventLogTranslator
     */
    private $subject;

    /**
     * @var TranslatorInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $translatorMock;

    public function setUp()
    {
        $this->translatorMock = $this->getMock('Symfony\Component\Translation\Translator', [], [], '', false);
        $this->subject        = $this->createService($this->translatorMock);
    }

    public function testTranslatorKeepsOriginalLocale()
    {
        $originalLocale = 'USAGI';
        $translator = new TranslatorHelper();
        $translator->setLocale($originalLocale);

        $service = $this->createService($translator);
        $entity  = new EventLog();

        $entity->setDescription('moo');

        $service->translate($entity);

        $this->assertSame($originalLocale, $translator->getLocale());

    }

    public function testTranslateWithDescriptionTranslatesDescription()
    {
        $expectedDescription = 'test';
        $expectedTranslation = 'TranslatedTest';

        $this->translatorMock->expects($this->once())
            ->method('trans')
            ->with($expectedDescription)
            ->will($this->returnValue($expectedTranslation));

        $entity = new EventLog();
        $entity->setDescription($expectedDescription);

        $this->subject->translate($entity);

        $this->assertInternalType('array', $entity->getTranslatedDescription());
        $this->assertArrayHasKey(
            'pl',
            $entity->getTranslatedDescription(),
            'Failed to assert that a "pl" translation exists'
        );

        $this->assertSame($expectedTranslation, $entity->getTranslatedDescription()['pl']);
    }

    public function testTranslateWithoutDescriptionDoesNotCallTranslator()
    {
        $entity = new EventLog();
        $entity->setDescription(null);

        $this->translatorMock->expects($this->never())
            ->method('trans');

        $this->subject->translate($entity);
    }

    /**
     * @param TranslatorInterface $translator
     * @return EventLogTranslator
     */
    private function createService(TranslatorInterface $translator)
    {
        return new EventLogTranslator($translator);
    }
}

/**
 * Helper class to perform assertions on the assigned locale (see EventLogTranslatorTest::testTranslatorKeepsOriginalLocale())
 */
class TranslatorHelper implements TranslatorInterface
{
    private $locale;

    public function trans($id, array $parameters = array(), $domain = null, $locale = null)
    {
    }

    public function transChoice($id, $number, array $parameters = array(), $domain = null, $locale = null)
    {
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

}