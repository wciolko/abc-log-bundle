<?php

declare(strict_types=1);

namespace Abc\Bundle\LogBundle\Exception;

use RuntimeException;

/**
 * Class UnsupportedSearchingCriteriaException
 * @package Abc\Bundle\LogBundle\Exception
 */
final class UnsupportedSearchingCriteriaException extends RuntimeException
{

}
