<?php

namespace Abc\Bundle\LogBundle\Model;

/**
 * Interface EventLogInterface
 *
 */
interface EventLogInterface
{
    /**
     * Get id
     *
     * @return int $id
     */
    public function getId();

    /**
     * Set type
     *
     * @param string $type
     * @return self
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType();

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description);

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription();


    /**
     * Set referenceId
     *
     * @param string $referenceId
     * @return self
     */
    public function setReferenceId($referenceId);

    /**
     * Get referenceId
     *
     * @return string $referenceId
     */
    public function getReferenceId();

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt);

    /**
     * Get createdAt
     *
     * @return \DateTime $createdAt
     */
    public function getCreatedAt();

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt);

    /**
     * Get updatedAt
     *
     * @return \DateTime $updatedAt
     */
    public function getUpdatedAt();

    /**
     * @return string
     */
    public function getCreatedBy();

    /**
     * @param string $createdBy
     */
    public function setCreatedBy($createdBy);

    /**
     * @return array
     */
    public function getParameters();

    /**
     * @param array $parameters
     */
    public function setParameters($parameters);

    /**
     * @return array
     */
    public function getTranslatedDescription();

    /**
     * @param array $translatedDescription
     */
    public function setTranslatedDescription(array $translatedDescription);

    /**
     * Returns the translated description for the given locale
     * or the original description if translation not found.
     *
     * @param string $locale
     * @return string
     */
    public function getTranslatedDescriptionForLocale($locale);

    /**
     * @return string
     */
    public function getSeverity();

    /**
     * @param string $severity
     * @return EventLog
     */
    public function setSeverity($severity);
}