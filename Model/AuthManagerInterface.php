<?php


namespace Abc\Bundle\LogBundle\Model;


use Symfony\Component\Security\Core\User\UserInterface;

interface AuthManagerInterface
{

    /**
     * @return UserInterface
     */
    public function getUser();
}