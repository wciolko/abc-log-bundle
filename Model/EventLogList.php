<?php

namespace Abc\Bundle\LogBundle\Model;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class EventList
 *
 */
class EventLogList
{
    /**
     * @var EventLog[]
     * @Type("array<Abc\Bundle\LogBundle\Entity\EventLog>")
     */
    protected $items;

    /**
     * @var int
     * @Type("integer")
     * @SerializedName("recordsTotal")
     */
    protected $total;

    /**
     * @var int
     * @Type("integer")
     * @SerializedName("recordsFiltered")
     */
    protected $filteredTotal;

    /**
     * @return int
     */
    public function getFilteredTotal()
    {
        return $this->filteredTotal;
    }

    /**
     * @param int $filteredTotal
     */
    public function setFilteredTotal($filteredTotal)
    {
        $this->filteredTotal = $filteredTotal;
    }

    /**
     * @return EventLog[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

}

