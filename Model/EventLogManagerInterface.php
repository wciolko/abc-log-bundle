<?php

namespace Abc\Bundle\LogBundle\Model;

interface EventLogManagerInterface
{
    /**
     * Returns an empty Entity instance.
     *
     * @return EventLogInterface
     */
    public function create();

    /**
     * Updates a Entity.
     *
     * @param EventLogInterface $item
     * @param bool              $andFlush
     */
    public function update(EventLogInterface $item, $andFlush = true);

    /**
     * Deletes a Entity.
     *
     * @param EventLogInterface $item
     * @return void
     */
    public function delete(EventLogInterface $item);

    /**
     * @param int $days
     *
     * @return mixed
     */
    public function deleteOlderThan(int $days);

    /**
     * Finds a Entity by the given criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     * @return \Traversable<EventLogInterface>
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * Finds a Entity by the given id.
     *
     * @param string $id
     * @return object
     */
    public function findById($id);

    /**
     * @return integer
     */
    public function getTotal();

    /**
     * @param array $criteria
     *
     * @return integer
     */
    public function getFilteredTotal(array $criteria);

    /**
     * Finds a Entity by the given criteria.
     *
     * @param array $criteria
     * @return EventLogInterface
     */
    public function findOneBy(array $criteria);

    /**
     * Returns a collection with all instances.
     *
     * @return \Traversable<EventLogInterface>
     */
    public function findAll();

    /**
     * Returns the Entity's fully qualified class name.
     *
     * @return string
     */
    public function getClass();

    /**
     * @param $referenceId
     * @param $type
     * @return int
     */
    public function getTotalByType($referenceId, $type);

}