<?php

namespace Abc\Bundle\LogBundle\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class EventLogSeverity
 * @package Abc\Bundle\Model\Enum
 * 
 * @method static EventLogSeverity INFO()
 * @method static EventLogSeverity ERROR()
 * @method static EventLogSeverity NOTICE()
 * @method static EventLogSeverity DEBUG()
 */
class EventLogSeverity extends Enum
{   
    const INFO    = 'INFO';
    const ERROR   = 'ERROR';
    const NOTICE  = 'NOTICE';
    const DEBUG   = 'DEBUG';
}