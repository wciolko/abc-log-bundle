<?php

namespace Abc\Bundle\LogBundle\Model\Enum;

use MyCLabs\Enum\Enum;

/**
 * EventLog Type enumeration.
 *
 * @method static EventLogType CLIENT()
 * @method static EventLogType EVENT()
 * @method static EventLogType LOGIN()
 * @method static EventLogType LOGOUT()
 * @method static EventLogType ERROR()
 *
 */
class EventLogType extends Enum
{
    const CLIENT = 'client';
    const EVENT  = 'event';
    const LOGIN  = 'login';
    const LOGOUT = 'logout';
    const ERROR  = 'error';
}