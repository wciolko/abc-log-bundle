<?php

namespace Abc\Bundle\LogBundle\Model;

abstract class EventLogManager implements EventLogManagerInterface
{

    /**
     * {@inheritDoc}
     */
    public function create()
    {
        $class = $this->getClass();

        /** @var EventLogInterface $item */
        $item = new $class;
        return $item;
    }
}
