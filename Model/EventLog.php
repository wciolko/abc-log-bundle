<?php

namespace Abc\Bundle\LogBundle\Model;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class EventLog
 */
class EventLog implements EventLogInterface
{
    /**
     * @var string $id
     * @Type("string")
     */
    protected $id;

    /**
     * @var string $type
     * @Type("string")
     */
    protected $type;

    /**
     * @var string $description
     * @Type("string")
     */
    protected $description;

    /**
     * @Type("array")
     * @SerializedName("translatedDescription")
     * @var array
     */
    protected $translatedDescription = [];

    /**
     * @var \DateTime $createdAt
     * @Type("DateTime<'Y-m-d\TH:i:s.uP'>")
     * @SerializedName("createdAt")
     */
    protected $createdAt;

    /**
     * @var \DateTime $updatedAt
     * @Type("DateTime")
     * @SerializedName("updatedAt")
     */
    protected $updatedAt;

    /**
     * @var string $referenceId
     * @Type("string")
     * @SerializedName("referenceId")
     */
    protected $referenceId;

    /**
     * @var string $createdBy
     * @Type("string")
     * @SerializedName("createdBy")
     */
    protected $createdBy;

    /**
     * @var array $parameters
     * @Type("array")
     */
    protected $parameters = [];

    /**
     * @var string $severity
     * @Type("string")
     */
    protected $severity = 'INFO';

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getTranslatedDescription()
    {
        return $this->translatedDescription;
    }

    /**
     * {@inheritdoc}
     */
    public function setTranslatedDescription(array $translatedDescription)
    {
        $this->translatedDescription = $translatedDescription;
    }

    /**
     * {@inheritdoc}
     */
    public function getTranslatedDescriptionForLocale($locale)
    {
        if (array_key_exists($locale, $this->translatedDescription)) {
            return $this->translatedDescription[$locale];
        }

        $params = [];
        foreach ($this->getParameters() as $key => $value) {
            $params['%' . $key . '%'] = $value;
        }

        return strtr($this->description, $params);
    }

    /**
     * {@inheritdoc}
     */
    public function getSeverity()
    {
        return $this->severity;
    }

    /**
     * {@inheritdoc}
     */
    public function setSeverity($severity)
    {
        $this->severity = $severity;
        return $this;
    }

    public function fromArray(array $data): EventLogInterface
    {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['typeValue'])) {
            $this->setType($data['typeValue']);
        }

        if (!empty($data['description'])) {
            $this->setDescription($data['description']);
        }

        if (!empty($data['translatedDescription'])) {
            $this->setTranslatedDescription(json_decode($data['translatedDescription'], true));
        }

        if (!empty($data['createdAt'])) {
            $this->setCreatedAt(\DateTime::createFromFormat('Y-m-d\TH:i:s.u', $data['createdAt']));
        }

        if (!empty($data['updatedAt'])) {
            $this->setUpdatedAt(\DateTime::createFromFormat('Y-m-d\TH:i:s.u', $data['updatedAt']));
        }

        if (!empty($data['referenceId'])) {
            $this->setReferenceId($data['referenceId']);
        }

        if (!empty($data['createdBy'])) {
            $this->setCreatedBy($data['createdBy']);
        }

        if (!empty($data['parameters'])) {
            $this->setParameters(json_decode($data['severity'], $data['parameters']));
        }

        if (!empty($data['severity'])) {
            $this->setSeverity($data['severity']);
        }

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id'                    => $this->id,
            'typeValue'                  => $this->getType(),
            'description'           => $this->getDescription(),
            'translatedDescription' => json_encode($this->getTranslatedDescription()),
            'createdAt'             => $this->getCreatedAt()->format('Y-m-d\TH:i:s.u'),
            'updatedAt'             => $this->getUpdatedAt()->format('Y-m-d\TH:i:s.u'),
            'referenceId'           => $this->getReferenceId(),
            'createdBy'             => $this->getCreatedBy(),
            'parameters'            => json_encode($this->getParameters()),
            'severity'              => $this->getSeverity(),
        ];
    }
}
