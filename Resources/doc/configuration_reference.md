Configuration Reference
=======================

All available configuration options are listed below with their default values.

.. code-block:: yaml

    abc_log:
        db_driver: orm
        model_manager_name: null
        service:
            manager: abc_log.manager.default
            event_log_manager: abc_log.event_log_manager.default