REST API
========

In order to use REST API you need to include routing file:

.. code-block:: yaml
    abc_log_api:
        type: rest
        resource: "@AbcLogBundle/Resources/config/routing.yml"
        prefix: /api
        
All the necessary documentation is available when you configure [NelmioApiDocBundle](https://github.com/nelmio/NelmioApiDocBundle/blob/master/Resources/doc/index.md)        