<?php

namespace Abc\Bundle\LogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class EventLogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', HiddenType::class);

        $builder->add('referenceId', TextType::class, [
                'label'    => 'log.label.referenceId',
                'required' => true,
            ]
        );

        $builder->add('type', TextType::class, [
                'label'    => 'log.label.type',
                'required' => true,
            ]
        );

        $builder->add('description', TextType::class, [
                'label'    => 'log.label.description',
                'required' => true,
            ]
        );

        $builder->add('parameters', TextType::class, [
                'label'    => 'log.label.parameters',
                'required' => false
            ]
        );

        $builder->add('createdBy', TextType::class, [
                'label'    => 'log.label.createdBy',
                'required' => false,
            ]
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'csrf_protection'    => false,
                'data_class'         => 'Abc\Bundle\LogBundle\Model\EventLog',
                'allow_extra_fields' => true,
            ]
        );
    }
}