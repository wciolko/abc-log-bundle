<?php

declare(strict_types=1);

namespace Abc\Bundle\LogBundle\ElasticSearch;

use Abc\Bundle\LogBundle\Exception\UnsupportedSearchingCriteriaException;
use Abc\Bundle\LogBundle\Model\EventLog;
use Abc\Bundle\LogBundle\Model\EventLogInterface;
use Abc\Bundle\LogBundle\Service\EventLogTranslatorInterface;
use Abc\Bundle\LogBundle\Model\EventLogManager as BaseEventLogManager;
use Doctrine\Common\Inflector\Inflector;
use JMS\Serializer\SerializerInterface;

/**
 * Class EventLogManager
 * @package Abc\Bundle\LogBundle\ElasticSearch
 */
class EventLogManager extends BaseEventLogManager
{
    const INDEX = 'event_logs';

    /** @var Client */
    protected $client;
    /** @var EventLogTranslatorInterface */
    protected $eventLogTranslator;
    /** @var SerializerInterface */
    protected $serializer;
    /** @var string */
    protected $indexUniqueId;

    /**
     * EventLogManager constructor.
     *
     * @param Client                      $client
     * @param EventLogTranslatorInterface $eventLogTranslator
     * @param SerializerInterface         $serializer
     * @param string                      $indexUniqueId
     */
    public function __construct(
        Client $client,
        EventLogTranslatorInterface $eventLogTranslator,
        SerializerInterface $serializer,
        string $indexUniqueId
    )
    {
        $this->client             = $client;
        $this->eventLogTranslator = $eventLogTranslator;
        $this->serializer         = $serializer;
        $this->indexUniqueId      = $indexUniqueId;
    }

    public function update(EventLogInterface $item, $andFlush = true)
    {
        $this->createIndex();

        if ($item instanceof EventLogInterface) {
            $this->eventLogTranslator->translate($item);
        }

        $params = [
            'index' => $this->getIndexName(),
            'id'    => $item->getId(),
            'body'  => $item->toArray(),
        ];

        $this->client->getInstance()->index($params);
    }

    public function bulkUpdate(array $items)
    {
        $this->createIndex();

        $params = [
            'body' => [],
        ];

        foreach ($items as $item) {
            if ($item instanceof EventLogInterface) {
                $this->eventLogTranslator->translate($item);
            }

            $params['body'][] = [
                'index' => [
                    '_index' => $this->getIndexName(),
                ],
            ];

            $params['body'][] = $item->toArray();

            unset($item);
        }

        $response = $this->client->getInstance()->bulk($params);
        unset($response);
        unset($params);
    }

    public function delete(EventLogInterface $item)
    {
        $params = [
            'index' => $this->getIndexName(),
            'id'    => $item->getId(),
        ];

        $this->client->getInstance()->delete($params);
    }

    public function deleteOlderThan(int $days)
    {
        // TODO: Implement deleteOlderThan() method.
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        if (count($criteria) > 0) {
            $params = [
                'index' => $this->getIndexName(),
                'body'  => [
                    'query' => [
                        'bool' => [
                            'must' => $this->prepareEsCriteria($criteria),
                        ],
                    ],
                ],
            ];
        } else {
            $params = [
                'index' => $this->getIndexName(),
                'body'  => [
                    'query' => [
                        'match_all' => new \stdClass(),
                    ],
                ],
            ];
        }

        if (is_numeric($limit)) {
            $params['body']['size'] = $limit;
        }

        if (is_numeric($offset)) {
            $params['body']['from'] = $offset;
        }

        if (is_array($orderBy) && $orderBy[key($orderBy)] !== 'id') {
            $params['body']['sort'] = [
                key($orderBy) => ['order' => strtolower($orderBy[key($orderBy)])],
            ];
        }

        $response = $this->client->getInstance()->search($params);
        $eventLog = null;

        $items = [];

        foreach ($response['hits']['hits'] as $hit) {
            $hit['_source']['id'] = $hit['_id'];
            $items[]              = (new EventLog())->fromArray($hit['_source']);
        }

        return $items;
    }

    public function findById($id)
    {
        $params = [
            'index' => $this->getIndexName(),
            'id'    => $id,
        ];

        $response = $this->client->getInstance()->get($params);
        $eventLog = null;

        if ($response['hits']['total']['value'] > 0) {
            $data       = $response['hits']['hits'][0]['_source'];
            $data['id'] = $response['hits']['hits'][0]['_id'];
            $eventLog   = (new EventLog())->fromArray($data);
        }

        return $eventLog;
    }

    public function getTotal()
    {
        $params = [
            'index' => $this->getIndexName(),
        ];

        return $this->client->getInstance()->count($params)['count'];
    }

    public function getFilteredTotal(array $criteria)
    {
        if (count($criteria) > 0) {
            $params = [
                'index' => $this->getIndexName(),
                'body'  => [
                    'query' => [
                        'bool' => [
                            'must' => $this->prepareEsCriteria($criteria),
                        ],
                    ],
                ],
            ];
        } else {
            $params = [
                'index' => $this->getIndexName(),
                'body'  => [
                    'query' => [
                        'match_all' => new \stdClass(),
                    ],
                ],
            ];
        }

        return $this->client->getInstance()->count($params)['count'];
    }

    public function findOneBy(array $criteria)
    {
        $params = [
            'index' => $this->getIndexName(),
            'body'  => [
                'from'  => 0,
                'size'  => 1,
                'query' => [
                    'bool' => [
                        'must' => $this->prepareEsCriteria($criteria),
                    ],
                ],
            ],
        ];

        $response = $this->client->getInstance()->search($params);
        $eventLog = null;

        if ($response['hits']['total']['value'] > 0) {
            $data       = $response['hits']['hits'][0]['_source'];
            $data['id'] = $response['hits']['hits'][0]['_id'];
            $eventLog   = (new EventLog())->fromArray($data);
        }

        return $eventLog;
    }

    public function findAll()
    {
        $result = $this->client->getInstance()->search([
            'index' => $this->getIndexName(),
            'body'  => [
                'query' => [
                    'match_all' => new \stdClass(),
                ],
            ],
        ]);

        $items = [];

        foreach ($result['hits']['hits'] as $hit) {
            $hit['_source']['id'] = $hit['_id'];
            $items[]              = (new EventLog())->fromArray($hit['_source']);
        }

        return $items;
    }

    public function getClass()
    {
        return EventLog::class;
    }

    public function getTotalByType($referenceId, $type)
    {
        $params = [
            'index' => $this->getIndexName(),
            'body'  => [
                'query' => [
                    'match' => [
                        'referenceId' => $referenceId,
                    ],
                ],
            ],
        ];

        $response = $this->client->getInstance()->count($params);

        return $response['count'];
    }

    public function createIndex()
    {
        if (!$this->client->getInstance()->indices()->exists(['index' => $this->getIndexName()])) {
            $this->client->getInstance()->indices()->create([
                'index' => $this->getIndexName(),
                'body'  => [
                    'mappings' => [
                        'properties' => [
                            "id"                    => [
                                "type"  => "text",
                                "index" => "false",
                            ],
                            "typeValue"            => [
                                "type"  => "text",
                                "index" => "true",
                            ],
                            "description"           => [
                                "type"       => "text",
                                "index"      => "true",
                            ],
                            "translatedDescription" => [
                                "type"  => "text",
                                "index" => "false",
                            ],
                            "createdAt"             => [
                                "type"   => "date",
                                "format" => "yyyy-MM-dd'T'HH:mm:ss.SSSSSS",
                                "index"  => "false",
                            ],
                            "updatedAt"             => [
                                "type"   => "date",
                                "format" => "yyyy-MM-dd'T'HH:mm:ss.SSSSSS",
                                "index"  => "false",
                            ],
                            "referenceId"           => [
                                "type"  => "text",
                                "index" => "true",
                            ],
                            "createdBy"             => [
                                "type"  => "text",
                                "index" => "false",
                            ],
                            "parameters"            => [
                                "type"  => "text",
                                "index" => "false",
                            ],
                            "severity"              => [
                                "type"  => "text",
                                "index" => "false",
                            ],
                        ],
                    ],
                ],
            ]);
        }
    }

    private function prepareEsCriteria(array $criteria): array
    {
        $esCriteria  = [];
        $criteria    = $this->camelizeKeys($criteria);
        $allowedKeys = [
            'id',
            'typeValue',
            'description',
            'translatedDescription',
            'createdAt',
            'updatedAt',
            'referenceId',
            'createdBy',
            'parameters',
            'severity',
        ];

        foreach ($criteria as $key => $value) {
            if (!in_array($key, $allowedKeys)) {
                throw new UnsupportedSearchingCriteriaException(sprintf('Unsupported field: %s', $key));
            }

            if ($key == 'translatedDescription') {
                $key = 'description';
            }

            $value = str_replace('/', '\/', $value);

            $esCriteria[] = [
                'query_string' => [
                    'default_field' => $key,
                    'query'         => ($key == 'depotId') ? (int)$value : ('*' . (string)$value . '*'),
                ],
            ];
        }

        return $esCriteria;
    }

    private function camelizeKeys(array $data): array
    {
        return array_combine(
            array_map(function ($itemValue) {
                if (is_integer($itemValue)) {
                    return $itemValue;
                } else {
                    return Inflector::camelize($itemValue);
                }
            }, array_keys($data)),
            array_map(function ($itemValue) {
                return is_array($itemValue) ? $this->camelizeKeys($itemValue) : $itemValue;
            }, $data)
        );
    }

    private function getIndexName(): string
    {
        return self::INDEX . '_' . $this->indexUniqueId;
    }
}
