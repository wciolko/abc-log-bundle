<?php

namespace Abc\Bundle\LogBundle\ElasticSearch;

use RuntimeException;

/**
 * Class ClientException
 * @package Abc\Bundle\LogBundle\ElasticSearch
 */
final class ClientException extends RuntimeException
{

}
