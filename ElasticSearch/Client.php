<?php

namespace Abc\Bundle\LogBundle\ElasticSearch;

use Aws\Credentials\CredentialProvider;
use Aws\Credentials\Credentials;
use Aws\ElasticsearchService\ElasticsearchPhpHandler;
use Aws\Exception\AwsException;
use Aws\Sts\StsClient;
use Elasticsearch\ClientBuilder;
use Predis\ClientInterface as RedisClientInterface;

/**
 * Class Client
 * @package Abc\Bundle\LogBundle\ElasticSearch
 */
class Client
{
    /** @var string */
    private $region;
    /** @var string */
    private $roleToAssumeArn;
    /** @var array */
    private $hosts;
    /** @var string */
    private $key;
    /** @var string */
    private $secret;
    /** @var RedisClientInterface */
    private $redisClient;

    public function __construct(
        string $region,
        string $roleToAssumeArn,
        array $hosts,
        string $key,
        string $secret,
        RedisClientInterface $redisClient
    )
    {
        $this->region          = $region;
        $this->roleToAssumeArn = $roleToAssumeArn;
        $this->hosts           = $hosts;
        $this->key             = $key;
        $this->secret          = $secret;
        $this->redisClient     = $redisClient;
    }

    public function getInstance()
    {
        $result = null;

        if (!empty($this->secret) && !empty($this->key)) {
            $stsClient = new StsClient([
                'region'      => $this->region,
                'version'     => '2011-06-15',
                'credentials' => [
                    'key'    => $this->key,
                    'secret' => $this->secret,
                ],
            ]);
        } else {
            $stsClient = new StsClient([
                'region'      => $this->region,
                'version'     => '2011-06-15',
            ]);
        }

        try {
            $token = $this->redisClient->get('aws_es_token');

            if (!$token) {
                $result = $stsClient->assumeRole([
                    'RoleArn'         => $this->roleToAssumeArn,
                    'RoleSessionName' => 'session1',
                    'DurationSeconds' => 3600,
                ]);
                $this->redisClient->set('aws_es_token', json_encode($result['Credentials']));
                $this->redisClient->expire('aws_es_token', 3595);
            } else {
                $result = ['Credentials' => json_decode($token, true)];
            }

            $provider = CredentialProvider::fromCredentials(
                new Credentials(
                    $result['Credentials']['AccessKeyId'],
                    $result['Credentials']['SecretAccessKey'],
                    $result['Credentials']['SessionToken']
                )
            );

            $handler = new ElasticsearchPhpHandler($this->region, $provider);
            $client  = ClientBuilder::create()
                ->setSSLVerification(true)
                ->setHandler($handler)
                ->setHosts($this->hosts)
                ->build();
        } catch (AwsException $e) {
            throw new ClientException($e->getMessage());
        }

        return $client;
    }
}
