<?php


namespace Abc\Bundle\LogBundle\Event;


use Abc\Bundle\LogBundle\Model\EventLogInterface;
use Abc\Bundle\LogBundle\Service\GeneralLogService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class LogSubscriber implements EventSubscriberInterface
{
    /** @var GeneralLogService */
    protected $logService;
    /** @var LoggerInterface */
    private $logger;

    /**
     * @param GeneralLogService    $logService
     * @param LoggerInterface|null $logger
     */
    function __construct(
        GeneralLogService $logService,
        LoggerInterface $logger = null)
    {
        $this->logService = $logService;
        $this->logger     = $logger == null ? new NullLogger() : $logger;
    }

    public static function getSubscribedEvents()
    {
        return [
            LogEvents::LOG_CREATE => ['logCreate', 0],
        ];
    }

    /**
     * @param LogEvent $event
     */
    public function logCreate(LogEvent $event)
    {
        $logEntry = $event->getEventLog();
        if ($logEntry instanceof EventLogInterface) {
            $this->logger->debug('Process client change event');

            try {
                $this->logger->info('EVENT log' . $logEntry->getDescription());
                $this->logService->logEventObject($logEntry);
            } catch (\Exception $e) {
                $this->logger->error('Not able to log event. ' . $e->getMessage());
            }
        }
    }
}