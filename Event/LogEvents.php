<?php


namespace Abc\Bundle\LogBundle\Event;


class LogEvents
{
    /**
     * The log.create event is thrown each time an order is modified
     * in the system.
     *
     * The event listener receives an
     * Abc\Bundle\LogBundle\Model\EventLogInterface
     *
     * @var string
     */
    const LOG_CREATE = 'log.create';


}