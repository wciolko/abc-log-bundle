<?php


namespace Abc\Bundle\LogBundle\Event;


use Abc\Bundle\LogBundle\Model\EventLogInterface;
use Symfony\Component\EventDispatcher\Event;

class LogEvent extends Event
{
    /** @var EventLogInterface */
    protected $eventLog;

    public function __construct(EventLogInterface $eventLog)
    {
        $this->eventLog = $eventLog;
    }

    public function getEventLog()
    {
        return $this->eventLog;
    }
}