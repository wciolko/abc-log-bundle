Abc Log Bundle
==========

##Overview

This bundle provides the following features:

- Abstract logging for given reference and type
- A REST-API to get to retrieve information about logs

##Disclaimer

Please note that this bundle is still under development. At the current moment we do not consider 
this bundle as stable and we feel free to change things at any level.

However we appreciate if you decide to use this bundle and we appreciate feedback and suggestions on improvements.

##Installation

The bundle should be installed through composer.

###Add the bundle to your composer.json file

```json
{
    "require": {
        "aboutcoders/log-bundle": "dev-master"
    }
}
```

###Update AppKernel.php of your Symfony Application

Add the following bundles to your kernel bootstrap sequence, in the `$bundles` array.

```php
public function registerBundles()
{
    $bundles = array(
        // ...
        new Abc\Bundle\LogBundle\AbcLogBundle(),
        // ...
    );
}
```

##Basic Configuration

At the current point only doctrine is supported as ORM. However by changing the configuration you can also use a different persistence layer.

####Configure doctrine orm

`All available configuration options are listed below with their default values.
 
.. code-block:: yaml
    abc_log:
      db_driver: orm


#####Register GEDMO Timestampable

The bundle makes use of the GEDMO Timestampable behavior. There are different on how you can set those behaviors up. Please refer to the [documentation](https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/symfony2.md) of library on how to do so.

##### Update the database schema

You finally need to update your database schema in order to create the tables required by the job bundle.

```bash
php app/console doctrine:schema:update --force
```

##Basic Usage

###Log event

In order to create a new log entry, you have to dispatch a new event.

####Dispatch a new event

You need to get Symfony `event_dispatcher` service from Service Container. Next thing is dispatching a proper event:

```php

$eventDispatcher->dispatch(LogEvents::LOG_CREATE, new LogEvent($logEntry));

```

##Further Documentation

- [Configuration Reference](./Resources/doc/configuration_reference.md)
- [REST](./Resources/doc/rest.md)