<?php

namespace Abc\Bundle\LogBundle\ElasticSearchV2;

use RuntimeException;

/**
 * Class ClientException
 * @package Abc\Bundle\LogBundle\ElasticSearch
 */
final class ClientException extends RuntimeException
{

}
