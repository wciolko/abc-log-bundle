<?php

declare(strict_types=1);

namespace Abc\Bundle\LogBundle\ElasticSearchV2;

use Elastica\Client as ElasticaClient;
use Elastica\Index as ElasticaIndex;
use Psr\Log\LoggerInterface;

final class Client extends ElasticaClient
{
    public function __construct(string $esHost, int $esPort, LoggerInterface $logger)
    {
        $config = [
            'host'      => $esHost,
            'port'      => $esPort,
            'transport' => 'https'
        ];

        parent::__construct($config, null, $logger);
    }

    public function getIndex(string $name): ElasticaIndex
    {
        return new ElasticaIndex($this, $name);
    }
}
