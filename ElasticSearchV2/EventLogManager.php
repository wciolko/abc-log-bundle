<?php

declare(strict_types=1);

namespace Abc\Bundle\LogBundle\ElasticSearchV2;

use Abc\Bundle\LogBundle\Exception\UnsupportedSearchingCriteriaException;
use Abc\Bundle\LogBundle\Model\EventLog;
use Abc\Bundle\LogBundle\Model\EventLogInterface;
use Abc\Bundle\LogBundle\Service\EventLogTranslatorInterface;
use Abc\Bundle\LogBundle\Model\EventLogManager as BaseEventLogManager;
use Doctrine\Common\Inflector\Inflector;
use JMS\Serializer\SerializerInterface;
use Elastica\Document;

final class EventLogManager extends BaseEventLogManager
{
    const INDEX = 'event_logs';

    /** @var Client */
    protected $client;
    /** @var EventLogTranslatorInterface */
    protected $eventLogTranslator;
    /** @var SerializerInterface */
    protected $serializer;
    /** @var string */
    protected $indexUniqueId;

    public function __construct(
        Client $client,
        EventLogTranslatorInterface $eventLogTranslator,
        SerializerInterface $serializer,
        string $indexUniqueId
    )
    {
        $this->client             = $client;
        $this->eventLogTranslator = $eventLogTranslator;
        $this->serializer         = $serializer;
        $this->indexUniqueId      = $indexUniqueId;
    }

    public function update(EventLogInterface $item, $andFlush = true)
    {
        if ($item instanceof EventLogInterface) {
            $this->eventLogTranslator->translate($item);
        }

        $this->client->getIndex($this->getIndexName())->addDocument(new Document($item->getId(), $item->toArray()));
    }

    public function delete(EventLogInterface $item)
    {
        $this->client->getIndex($this->getIndexName())->deleteDocuments([new Document($item->getId())]);
    }

    public function deleteOlderThan(int $days)
    {
        // TODO: Implement deleteOlderThan() method.
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        if (count($criteria) > 0) {
            $params = [
                'query' => [
                    'bool' => [
                        'must' => $this->prepareEsCriteria($criteria),
                    ],
                ],
            ];
        } else {
            $params = [
                'query' => [
                    'match_all' => new \stdClass(),
                ],
            ];
        }

        if (is_numeric($limit)) {
            $params['size'] = $limit;
        }

        if (is_numeric($offset)) {
            $params['from'] = $offset;
        }

        if (is_array($orderBy) && $orderBy[key($orderBy)] !== 'id') {
            $params['sort'] = [
                key($orderBy) => ['order' => strtolower($orderBy[key($orderBy)])],
            ];
        }

        $response = $this->client->getIndex($this->getIndexName())->search($params);
        $data     = $response->getResponse()->getData();
        $eventLog = null;

        $items = [];

        foreach ($data['hits']['hits'] as $hit) {
            $hit['_source']['id'] = $hit['_id'];
            $items[]              = (new EventLog())->fromArray($hit['_source']);
        }

        return $items;
    }

    public function findById($id)
    {
        $params = [
            'id' => $id,
        ];

        $response = $this->client->getIndex($this->getIndexName())->get($params);
        $response = $response->getResponse()->getData();
        $eventLog = null;

        if ($response['hits']['total']['value'] > 0) {
            $data       = $response['hits']['hits'][0]['_source'];
            $data['id'] = $response['hits']['hits'][0]['_id'];
            $eventLog   = (new EventLog())->fromArray($data);
        }

        return $eventLog;
    }

    public function getTotal()
    {
        $params = [];

        return $this->client->getIndex($this->getIndexName())->count($params);
    }

    public function getFilteredTotal(array $criteria)
    {
        if (count($criteria) > 0) {
            $params = [
                'query' => [
                    'bool' => [
                        'must' => $this->prepareEsCriteria($criteria),
                    ],
                ],
            ];
        } else {
            $params = [
                'query' => [
                    'match_all' => new \stdClass(),
                ],
            ];
        }

        return $this->client->getIndex($this->getIndexName())->count($params);
    }

    public function findOneBy(array $criteria)
    {
        $params = [
            'from'  => 0,
            'size'  => 1,
            'query' => [
                'bool' => [
                    'must' => $this->prepareEsCriteria($criteria),
                ],
            ],
        ];

        $response = $this->client->getIndex($this->getIndexName())->search($params);
        $response = $response->getResponse()->getData();
        $eventLog = null;

        if ($response['hits']['total']['value'] > 0) {
            $data       = $response['hits']['hits'][0]['_source'];
            $data['id'] = $response['hits']['hits'][0]['_id'];
            $eventLog   = (new EventLog())->fromArray($data);
        }

        return $eventLog;
    }

    public function findAll()
    {
        $result = $this->client->getIndex($this->getIndexName())->search([
            'query' => [
                'match_all' => new \stdClass(),
            ],
        ]);

        $data  = $result->getResponse()->getData();
        $items = [];

        foreach ($data['hits']['hits'] as $hit) {
            $hit['_source']['id'] = $hit['_id'];
            $items[]              = (new EventLog())->fromArray($hit['_source']);
        }

        return $items;
    }

    public function getClass()
    {
        return EventLog::class;
    }

    public function getTotalByType($referenceId, $type)
    {
        $params = [
            'query' => [
                'match' => [
                    'referenceId' => $referenceId,
                ],
            ],
        ];

        return $this->client->getIndex($this->getIndexName())->count($params);
    }

    private function prepareEsCriteria(array $criteria): array
    {
        $esCriteria  = [];
        $criteria    = $this->camelizeKeys($criteria);
        $allowedKeys = [
            'id',
            'typeValue',
            'description',
            'translatedDescription',
            'createdAt',
            'updatedAt',
            'referenceId',
            'createdBy',
            'parameters',
            'severity',
        ];

        foreach ($criteria as $key => $value) {
            if (!in_array($key, $allowedKeys)) {
                throw new UnsupportedSearchingCriteriaException(sprintf('Unsupported field: %s', $key));
            }

            if ($key == 'translatedDescription') {
                $key = 'description';
            }

            $value = str_replace('/', '\/', $value);

            if (
                $key === 'description' &&
                is_string($value) &&
                strpos(trim($value), ' ') !== false
            ) {
                $value = str_replace(' ', ' AND ', trim($value));
            }

            $esCriteria[] = [
                'query_string' => [
                    'default_field' => $key,
                    'query'         => ($key == 'depotId') ? (int)$value : ('*' . (string)$value . '*'),
                ],
            ];
        }

        return $esCriteria;
    }

    private function camelizeKeys(array $data): array
    {
        return array_combine(
            array_map(function ($itemValue) {
                if (is_integer($itemValue)) {
                    return $itemValue;
                } else {
                    return Inflector::camelize($itemValue);
                }
            }, array_keys($data)),
            array_map(function ($itemValue) {
                return is_array($itemValue) ? $this->camelizeKeys($itemValue) : $itemValue;
            }, $data)
        );
    }

    private function getIndexName(): string
    {
        return $this->indexUniqueId . '-' . self::INDEX;
    }
}
