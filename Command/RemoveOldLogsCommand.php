<?php

declare(strict_types=1);

namespace Abc\Bundle\LogBundle\Command;

use Abc\Bundle\LogBundle\Model\EventLogManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class RemoveOldLogsCommand
 *
 * @package Abc\Bundle\LogBundle\Command
 */
class RemoveOldLogsCommand extends Command
{
    /** @var int */
    protected        $defaultPeriod = 90;
    protected static $defaultName   = 'abc:log:remove-old-logs';

    /** @var EventLogManagerInterface */
    protected $eventLogManager;

    public function __construct(EventLogManagerInterface $eventLogManager)
    {
        $this->eventLogManager = $eventLogManager;

        parent::__construct(static::getDefaultName());
    }

    protected function configure()
    {
        $this
            ->setDescription('Remove logs older than provided number of days. Default value: ' . $this->defaultPeriod)
            ->addArgument('days', InputArgument::OPTIONAL, 'Number of days', $this->defaultPeriod);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $quantityOfDays = (int)$input->getArgument('days');

        if ($quantityOfDays === 0) {
            $helper   = $this->getHelper('question');
            $question = new ConfirmationQuestion('Do you want to remove all logs older than ' . $quantityOfDays . ' days?', false);

            if (!$helper->ask($input, $output, $question)) {
                return Command::SUCCESS;
            }
        }

        $this->eventLogManager->deleteOlderThan($quantityOfDays);

        $output->writeln('<info>Logs removed.</info>');
        return Command::SUCCESS;
    }
}
