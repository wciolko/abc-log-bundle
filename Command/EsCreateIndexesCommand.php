<?php

declare(strict_types=1);

namespace Abc\Bundle\LogBundle\Command;

use Abc\Bundle\LogBundle\Model\EventLogManagerInterface;
use Elasticsearch\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class EsCreateIndexesCommand
 * @package Abc\Bundle\LogBundle\Command
 */
class EsCreateIndexesCommand extends Command
{
    /** @var string */
    protected static $defaultName = 'abc:log:es:create-indexes';
    /** @var Client */
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;

        parent::__construct(static::getDefaultName());
    }

    protected function configure()
    {
        $this
            ->setDescription('Create indexes in Elastic Search');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->client->indices()->putIndexTemplate([
            "index_patterns" => ["event_logs*"],
            'priority' => 1,
            'template' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 0,
                ],
                'mappings' => [
                    '_source' => [
                        'enabled' => true
                    ],
                    'properties' => [
                        'referenceId' => [
                            'type' => 'keyword'
                        ],
                    ]
                ]
            ]
        ]);

        return Command::SUCCESS;
    }
}
