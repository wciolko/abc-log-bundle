<?php

namespace Abc\Bundle\LogBundle;

use Abc\Bundle\LogBundle\Doctrine\Types\DateTimeMillisecondsType;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AbcLogBundle extends Bundle
{
    public function boot()
    {
        if (!Type::hasType('datetime_milliseconds')) {
            Type::addType('datetime_milliseconds', DateTimeMillisecondsType::class);
        }
    }
}
