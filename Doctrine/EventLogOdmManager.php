<?php

namespace Abc\Bundle\LogBundle\Doctrine;

use Abc\Bundle\LogBundle\Model\EventLogInterface;
use Abc\Bundle\LogBundle\Model\EventLogManager as BaseEventLogManager;
use Abc\Bundle\LogBundle\Service\EventLogTranslatorInterface;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use DateTimeImmutable;

class EventLogOdmManager extends BaseEventLogManager
{
    /** @var ManagerRegistry */
    protected $objectManager;

    /** @var string */
    protected $class;

    /** @var DocumentRepository */
    protected $repository;

    /** @var EventLogTranslatorInterface */
    private $eventLogTranslator;

    /**
     * Constructor.
     *
     * @param ManagerRegistry             $om
     * @param string                      $class
     * @param EventLogTranslatorInterface $eventLogTranslator
     */
    public function __construct(ManagerRegistry $om, $class, EventLogTranslatorInterface $eventLogTranslator)
    {
        $this->objectManager      = $om->getManager();
        $this->repository         = $om->getRepository($class);
        $this->eventLogTranslator = $eventLogTranslator;
        $metadata                 = $om->getClassMetadata($class);
        $this->class              = $metadata->getName();
    }

    /**
     * {@inheritDoc}
     */
    public function findBySlug($slug)
    {
        return $this->findOneBy(['slug' => $slug]);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(EventLogInterface $node)
    {
        $this->objectManager->remove($node);
        $this->objectManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function deleteOlderThan(int $days)
    {
        $createdAt = new DateTimeImmutable("-$days days");

        $qb = $this->getQueryBuilder();
        $qb->delete();
        $qb->where($this->getTableName() . '.createdAt < :createdAt');
        $qb->setParameter('createdAt', $createdAt);

        $qb->getQuery()->execute();
    }

    /**
     * {@inheritDoc}
     */
    public function update(EventLogInterface $node, $andFlush = true)
    {
        if ($node instanceof EventLogInterface) {
            $this->eventLogTranslator->translate($node);
        }

        $this->objectManager->persist($node);

        if ($andFlush) {
            $this->objectManager->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * {@inheritDoc}
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $query = $this->getQueryBuilder();
        $query = $this->getMatchingQueryForCriteria($query, $criteria);

        if (isset($orderBy)) {
            foreach ($orderBy as $key => $direction) {
                if (strpos($key, '.') > 0) {
                    $query->addOrderBy($key, $direction);
                } else {
                    $query->addOrderBy($this->getTableName() . '.' . $key, $direction);
                }
            }
        }

        if (isset($limit)) {
            $query->setMaxResults($limit);
        }

        if (isset($offset)) {
            $query->setFirstResult($offset);
        }

        $query->groupBy($this->getTableName() . '.id');

        return $query->getQuery()->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        $qb = $this->getQueryBuilder();
        $qb->select('count(' . $this->getTableName() . '.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getFilteredTotal(array $criteria)
    {
        $qb = $this->getQueryBuilder();
        $qb->select('count(' . $this->getTableName() . '.id)');

        $qb = $this->getMatchingQueryForCriteria($qb, $criteria);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param string $referenceId
     * @param string $type
     * @return int
     */
    public function getTotalByType($referenceId, $type)
    {
        $qb = $this->getQueryBuilder();
        $qb->where($this->getTableName() . '.referenceId = :referenceId AND ' . $this->getTableName() . '.type = :type');
        $qb->setParameter(':referenceId', $referenceId);
        $qb->setParameter(':type', $type);
        $qb->select('count(' . $this->getTableName() . '.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * {@inheritDoc}
     */
    public function findById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * {@inheritDoc}
     */
    protected function getQueryBuilder()
    {
        return $this->repository->createQueryBuilder($this->getTableName());
    }

    /**
     * {@inheritDoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    public function getTableName()
    {
        return 'el';
    }

    /**
     * @param QueryBuilder $query
     * @param array        $criteria
     *
     * @return QueryBuilder
     */
    protected function getMatchingQueryForCriteria($query, array $criteria)
    {
        foreach ($criteria as $key => $value) {
            if (!in_array($key, ['referenceId', 'type'])) {
                $operator = ' LIKE :%s';
                $value = '%' . $value . '%';
            } else {
                $operator = ' = :%s';
            }

            $query->andWhere($this->getTableName() . '.' . $key . sprintf($operator, $key))
                ->setParameter($key, $value);
        }

        return $query;
    }
}