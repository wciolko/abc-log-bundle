<?php

namespace Abc\Bundle\LogBundle\Doctrine\Types;

use DateTime;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

class DateTimeMillisecondsType extends Type
{
    const NAME = 'datetime_milliseconds';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'DATETIME(3)';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof DateTime) {
            return $value;
        }

        return new DateTime($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof DateTime) {
            return $value->format('Y-m-d H:i:s.u');
        } else {
            throw ConversionException::conversionFailedInvalidType($value, $this->getName(), ['DateTime']);
        }
    }

    public function getName()
    {
        return self::NAME;
    }
}